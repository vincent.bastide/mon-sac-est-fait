/**
 * @file heuristic.h
 * @author Grp SacEstFait
 * @brief Remplissage du sac
 */

#ifndef HEURISTIC_H
#define HEURISTIC_H

#include "./../include/item.h"
#include "./../include/instance.h"
#include "./../include/parser.h"
#include "./../include/sort.h" 

/**
 * @brief Rempli le sac de manière dynamique ou non à partir d'une méthode donnée
 * @param parser 
 * @param sortedItemList 
 * @param dynamical 
 * @param idInstance 
 * @return Item** 
 */
Item** fillBag(Parser *parser, Item **sortedItemList, int dynamical, int idInstance);

#endif 