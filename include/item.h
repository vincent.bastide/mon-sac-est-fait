/**
 * @file item.h
 * @author Grp SacEstFait
 * @brief Fonctions permettant de gérer les items
 */

#ifndef ITEM_H
#define ITEM_H

#include <stdio.h>
#include <stdlib.h>

typedef struct {
	int value;
	int *weight;
    int included;
} Item;

/**
 * @brief Crée un item
 * @param nbDimensions 
 * @return Item* 
 */
Item* Item_new(int nbDimensions);

/**
 * @brief Initialise un item
 * @param item 
 * @param nbDimensions 
 * @return int 
 */
int Item_init(Item *item, int nbDimensions);

/**
 * @brief Libère les éléments de la structure Item
 * @param item 
 */
void Item_finalize(Item *item);

/**
 * @brief Supprime un Item
 * @param item 
 */
void Item_delete(Item *item);

/**
 * @brief Récupère la valeur de l'item
 * @param item 
 * @return int 
 */
int Item_getValue(Item *item);

/**
 * @brief Récupère le poids de l'item
 * @param item 
 * @param dimensionNumber 
 * @return int 
 */
int Item_getWeight(Item *item, int dimensionNumber);

/**
 * @brief Permet d'inclure ou non un item
 * @param item 
 * @param boolean 
 */
void setIncluded(Item *item, int boolean);

#endif