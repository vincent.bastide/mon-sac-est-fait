/**
 * @file instance.h
 * @author Grp SacEstFait
 * @brief Fonctions permettant de gérer les instances
 */

#ifndef INSTANCE_H
#define INSTANCE_H

#include <stdio.h>
#include <stdlib.h>
#include "./../include/item.h"
#include "./../include/timer.h"
#include "./../include/filesystem.h"

typedef struct {
	int nbItems;
	int nbDimensions;
	Item **itemList;
	int *maxWeight;
	int initTime;
		
} Instance;

/**
 * @brief Crée une instance
 * @param nbItems 
 * @param nbDimensions 
 * @param fileIterator 
 * @return Instance* 
 */
Instance* Instance_new(int nbItems, int nbDimensions, FileIterator *fileIterator);

/**
 * @brief Initialise une instance
 * @param instance 
 * @param nbItems 
 * @param nbDimensions 
 * @param fileiterator 
 * @return int 
 */
int Instance_init(Instance *instance, int nbItems, int nbDimensions, FileIterator *fileiterator);

/**
 * @brief Libère les éléments de la structure
 * @param instance 
 */
void Instance_finalize(Instance *instance);

/**
 * @brief Supprime l'instance
 * @param instance 
 */
void Instance_delete(Instance *instance);

/**
 * @brief Récupère le nombre d'items de l'instance
 * @param instance 
 * @return int 
 */
int Instance_getNbItems(Instance *instance);

/**
 * @brief Récupère le nombre de dimensions dans l'instance
 * @param instance 
 * @return int 
 */
int Instance_getNbDimensions(Instance *instance);

/**
 * @brief Copie une instance vers une autre allouée sur le tas
 * @param itemList 
 * @param nbItems 
 * @param nbDimensions 
 * @return Item** 
 */
Item** Instance_copyItemList(Item **itemList,int nbItems,int nbDimensions);

/**
 * @brief Supprime la liste d'item d'une instance
 * @param itemList 
 * @param nbItems 
 */
void Instance_ItemList_Delete(Item** itemList,int nbItems);

#endif