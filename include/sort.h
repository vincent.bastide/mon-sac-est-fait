/**
 * @file sort.h
 * @author Grp SacEstFait
 * @brief Méthodes de tri des items
 */

#ifndef SORT_H
#define SORT_H

#include <math.h>
#include "./../include/instance.h"
#include "./../include/item.h"
#include "./../include/tools.h"

/**
 * @brief Tri une liste d'item par ordre décroissant
 * @param itemList 
 * @param array 
 * @param nbItems 
 */
void decreasingSortItem(Item **itemList, float *array, int nbItems);

/**
 * @brief Récupère le poids total des objets pour une dimension donnée
 * @param dimension 
 * @param itemList 
 * @param nbItems 
 * @return int 
 */
int getTotalWeight(int dimension, Item **itemList, int nbItems);

// Aléatoire

/**
 * @brief Tri par valeur décroissante
 * @param instance 
 * @return Item** 
 */
Item** decreasingOrder(Instance *instance);

/**
 * @brief Ratio valeur des objets et somme des poids de toutes les dimensions
 * @param instance 
 * @return Item** 
 */
Item** ratioOrderSum(Instance *instance);

/**
 * @brief Ratio valeur des objets et poids dimension la plus critique
 * @param instance 
 * @return Item** 
 */
Item** ratioOrderCritical(Instance *instance);

/**
 * @brief Tri par ordre croissant
 * @param instance 
 * @return Item** 
 */
Item** increasingOrderItem(Instance *instance);

/**
 * @brief Tri aléatoire
 * @param instance 
 * @return Item** 
 */
Item** randomSort(Instance *instance);
#endif