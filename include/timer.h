/**
 * @file timer.h
 * @author Grp SacEstFait
 * @brief Timer utilisé pour connaître le temps d'init. d'une instance
 */

#ifndef TIMER_H
#define TIMER_H

#include <time.h>

#define CLOCKS_PER_MS (CLOCKS_PER_SEC / 1000)

typedef struct {
	clock_t startTime;
	clock_t stopTime;
} Timer;

/**
 * @brief Démarre le timer
 * @param timer 
 */
void Timer_start(Timer *timer);

/**
 * @brief Arrête le timer
 * @param timer 
 */
void Timer_stop(Timer *timer);

/**
 * @brief Récupère le temps écoulé du timer
 * @param timer 
 * @return int 
 */
int Timer_getElapsedTime(Timer *timer);

#endif