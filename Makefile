CC=gcc
LINKER=gcc -o
CFLAGS=-Wall -Werror
LDFLAGS=-Wall
EXEC=SacEstFait
SRCDIR=src
INCDIR=include
OBJDIR=obj


SOURCES:=$(wildcard $(SRCDIR)/*.c)
INCLUDES:=$(wildcard $(INCDIR)/*.h)
OBJECTS:=$(SOURCES:$(SRCDIR)/%.c=$(OBJDIR)/%.o)

all: $(EXEC)

$(EXEC): $(OBJECTS)
	@echo linking : $^
	@$(CC) -o $@ $(LDFLAGS) $(OBJECTS)
	@echo "Linking successfully"

$(OBJECTS): $(OBJDIR)/%.o: $(SRCDIR)/%.c
	@echo Compiling $<
	@$(CC) $(CFLAGS) -c $< -o $@
	@echo "Compiling successfully"

.PHONY: clean mrproper run valgrind

clean:
	rm -rf $(OBJECTS)

mrproper: clean
	rm -rf $(EXEC)

run: $(EXEC)
	@echo ""
	@./$(EXEC)

valgrind: all
	valgrind --tool=memcheck --leak-check=full ./$(EXEC)
