# Use official ubuntu image
FROM ubuntu

# Workdir
WORKDIR /app

# Install dependencies
RUN apt update && apt install -y \
		make \
		gcc \
		check \
		curl \
		wget \
		valgrind
