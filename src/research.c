#include "./../include/research.h"


Item** localResearch(Parser * parser,int t){
    int nbItems = Instance_getNbItems(parser->instanceArray[t]);
    int nbDimensions = Instance_getNbDimensions(parser->instanceArray[t]);
    Item** currentSolution = Instance_copyItemList(parser->instanceArray[t]->itemList, nbItems, nbDimensions);
    Item** bestSolution = Instance_copyItemList(parser->instanceArray[t]->itemList, nbItems, nbDimensions);
    Item** bestNeighborSol = Instance_copyItemList(parser->instanceArray[t]->itemList, nbItems, nbDimensions);
    
    int bestValue = objectiveFunction(parser->instanceArray[t]->itemList, nbItems);
    int currentF = bestValue;
    int prec = currentF;
    int continu = 1;
   
    while (continu == 1) {
        int bestVoisin = 0;
        //Détermination des sol au voisinage
        // opérateur 1 
        for (int i = 0; i < nbItems; i++) {
            if (currentSolution[i]->included == 0) {
                currentSolution[i]->included = 1;
                if (checkSolution(parser->instanceArray[t], currentSolution) == 1) {
                    if (objectiveFunction(currentSolution, nbItems) > bestVoisin) {
                        Instance_ItemList_Delete(bestNeighborSol, nbItems);
                        bestNeighborSol = Instance_copyItemList(currentSolution, nbItems, nbDimensions);
                        bestVoisin = objectiveFunction(bestNeighborSol, nbItems);
                    }
                }
                currentSolution[i]->included = 0;
            }
        }
       
        // opérateur 2
        for (int i = 0; i < nbItems; i++) {
            if (currentSolution[i]->included == 1) {
                currentSolution[i]->included = 0;
                for (int j = 0; j < nbItems; j++) {
                    if (currentSolution[j]->included == 0 && j != i) {
                        currentSolution[j]->included = 1;
                        if (checkSolution(parser->instanceArray[t], currentSolution)) {
                            if (objectiveFunction(currentSolution, nbItems) > bestVoisin) {
                                Instance_ItemList_Delete(bestNeighborSol, nbItems);
                                bestNeighborSol = Instance_copyItemList(currentSolution, nbItems, nbDimensions);
                                bestVoisin = objectiveFunction(currentSolution, nbItems);
                            }
                        }
                        currentSolution[j]->included = 0; 
                    }
                }
                currentSolution[i]->included = 1;
            }
        }
       
        currentF = bestVoisin;
        Instance_ItemList_Delete(currentSolution, nbItems);
        currentSolution = Instance_copyItemList(bestNeighborSol, nbItems, nbDimensions);
      
        if (currentF > bestValue) {
            bestValue = currentF;
            Instance_ItemList_Delete(bestSolution, nbItems);
            bestSolution = Instance_copyItemList(currentSolution, nbItems, nbDimensions);
        } else {
            if (currentF <= prec) continu = 0;
        }
        prec = currentF;

    }
    Instance_ItemList_Delete(currentSolution, nbItems);
    Instance_ItemList_Delete(bestNeighborSol, nbItems);
    return bestSolution;
}
