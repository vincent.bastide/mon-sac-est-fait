#include "./../include/sort.h"

Item**  randomSort(Instance *instance){
    srand(time(NULL));
    int nbItems = Instance_getNbItems(instance);
    int alea = 0;
    int *indicesArray = (int*) malloc(sizeof(int) * nbItems);
    int *permutation = (int*) malloc(sizeof(int) * nbItems);
    
    for (int i=0; i < nbItems; i++) {
        indicesArray[i] = i;
    }

    for (int i = 0; i < instance->nbItems; i++) {
        alea = rand() % nbItems;
        permutation[i] = indicesArray[alea];
        --nbItems;
        for (int j = alea;  j < nbItems; j++) {
            indicesArray[j]= indicesArray[j+1];
        }
    }
    Item *tmpItem;
    
    for(int i = 0 ; i < Instance_getNbItems(instance);i++) {
        tmpItem = instance->itemList[i];
        instance->itemList[i]=instance->itemList[permutation[i]];
        instance->itemList[permutation[i]] = tmpItem;
    }
    
    free(indicesArray);
    free(permutation);
   
    return instance->itemList;

}
void decreasingOrderItem(Item **itemList, float *array, int nbItems) {
    Item *tmpItem;
    float tmpFloat;

    for (int i = 0; i < nbItems; i++) {
        for (int j = 0; j < nbItems; j++) {
            if (array[i] > array[j]) {
                tmpItem = itemList[i];
                itemList[i] = itemList[j];
                itemList[j] = tmpItem;
                
                tmpFloat = array[i];
                array[i] = array[j];
                array[j] = tmpFloat;
            }
        }   
    }
}

int getTotalWeight(int dimension, Item **itemList, int nbItems){
    int res = 0;
    for (int i = 0; i < nbItems; i++) {  
       if (itemList[i]->included == 0) {
           res += itemList[i]->weight[dimension];
       }
    }
    return res;
} 


// Tri par valeur décroissante
Item** decreasingOrder(Instance *instance) {
    int nbItems = Instance_getNbItems(instance);
    Item *tmpItem;

    for (int i = 0; i < nbItems; i++) {
        for (int j = 0; j < nbItems; j++) {
            if (instance->itemList[i]->value > instance->itemList[j]->value) {
                tmpItem = instance->itemList[i];
                instance->itemList[i] = instance->itemList[j];
                instance->itemList[j] = tmpItem;
            }
        }
    }
    return instance->itemList;
}

// Tri décroissant en fonction du ratio valeur de l'objet / somme des poids de l'objet
Item** ratioOrderSum(Instance *instance) {
    int nbDimensions = Instance_getNbDimensions(instance);
    int nbItems = Instance_getNbItems(instance);
    float ratioArray[nbItems];
    int res = 0;

    for (int i = 0; i < nbItems; i++) {
        for (int j = 0 ; j < nbDimensions; j++){
             res += instance->itemList[i]->weight[j];
        }
        // Remplissage du tableau de ratio (valeur de l'objet / somme des poids de l'objet) 
        ratioArray[i] = (float) instance->itemList[i]->value /  res;
        res = 0;
    }
    decreasingOrderItem(instance->itemList, ratioArray, nbItems);
    return instance->itemList;
}

// Tri décroissant en fonction  du ratio valeur de l'objet / poids de l'objet dans la dimension critique
Item** ratioOrderCritical(Instance *instance) {
    int nbDimensions = Instance_getNbDimensions(instance);
    int nbItems = Instance_getNbItems(instance);
    float criticalArray[nbDimensions];
    float itemRatioArray[nbItems];
    int criticalDimension = 0;

    //Détermination de la dimension critique
    for (int i = 0; i < nbDimensions; i++) {
       criticalArray[i] = (float) getTotalWeight(i, instance->itemList,nbItems) / (float) instance->maxWeight[i] ;
    }
    criticalDimension = posMax(criticalArray,nbDimensions);

    //Remplissage du tableau de ratio objet
    for(int i = 0 ; i < nbItems ; i++){
        if(instance->itemList[i]->included == 0){
            itemRatioArray[i] = (float) instance->itemList[i]->value / instance->itemList[i]->weight[criticalDimension];
        } else {
            itemRatioArray[i] = 0;
        }
    }
    decreasingOrderItem(instance->itemList, itemRatioArray, nbItems);
    return instance->itemList;
}

// Notre tri (Ordre croissant)
Item** increasingOrderItem(Instance *instance) {
    int nbItems = Instance_getNbItems(instance);
    Item *tmpItem;

    for (int i = 0; i < nbItems; i++) {
        for (int j = 0; j < nbItems; j++) {
            if (instance->itemList[i]->value < instance->itemList[j]->value) {
                tmpItem = instance->itemList[i];
                instance->itemList[i] = instance->itemList[j];
                instance->itemList[j] = tmpItem;
            }
        }
    }
    return instance->itemList;
}