/**
 * @file main.c
 * @author Vincent BASTIDE / Guillaume CIRET 
 * @brief 
 */

#include <stdio.h>
#include <stdlib.h>
#include "./../include/parser.h"
#include "./../include/interface.h"

int main(int argc, char **argv) {
    FILE *file;
    file = fopen("test0.txt", "r");
    if (file) {
        Parser* parser = Parser_new(file);
        interface(parser);
        Parser_delete(parser);
    }
    fclose(file);
    return EXIT_SUCCESS;
}
