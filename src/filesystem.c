#include "./../include/filesystem.h"

FileIterator* FileIterator_new(FILE* file){
    FileIterator *newfileIterator = (FileIterator*) malloc(sizeof(FileIterator));
    if( newfileIterator){
        if (FileIterator_init(newfileIterator, file)) {
            free(newfileIterator);
            newfileIterator = NULL;
        } 
    
    }
    return newfileIterator;
}

int FileIterator_init(FileIterator *fileIterator , FILE *file){
     if (file) {
        fileIterator->file = file; 
        fileIterator->current = (char*) malloc(sizeof(char) * BUFFER_SIZE);
        FileIterator_next(fileIterator); 
        return 0;
    }
    return 1;
}

void FileIterator_finalize(FileIterator *fileIterator) {
    if (fileIterator) {
        free(fileIterator->current);
    }
}

void FileIterator_delete(FileIterator* fileIterator){
    if (fileIterator) {
        FileIterator_finalize(fileIterator);
        free(fileIterator);
    }
}

int FileIterator_hasNext(FileIterator* fileIterator){
     return fileIterator->current != NULL;
}

void FileIterator_next(FileIterator* fileIterator){
    if (fileIterator) fgets(fileIterator->current, BUFFER_SIZE, fileIterator->file);
}

const char* FileIterator_get(FileIterator* fileIterator){
    return (const char*) fileIterator->current;
}

int FileIterator_getNbInstance(FileIterator* fileIterator){
    int nbInstances = 0;
    if (fileIterator) sscanf(FileIterator_get(fileIterator), "%d", &nbInstances);
    return nbInstances;
}

int FileIterator_getNbItems(FileIterator * fileIterator){
    int nbItems = 0;
    if (fileIterator) fscanf(fileIterator->file,"%d",&nbItems);
    return nbItems;
}

int FileIterator_getNbDimensions(FileIterator* fileIterator){
    int dimension = 0;
     if(fileIterator) fscanf(fileIterator->file,"%d",&dimension);
    return dimension;
}

int FileIterator_nextInt(FileIterator * fileIterator){
    int integer = 0;
    if(fileIterator) fscanf(fileIterator->file,"%d",&integer);
    return integer;
}
