#include "./../include/interface.h"

void printChoiceOfSorting() {
    printf("\nChoix de la méthode de tri:\n"
        "1] Tri aléatoire\n"
        "2] Tri décroissant\n"
        "3] Tri ratio valeur objet / poids des dimensions\n"
        "4] Tri ratio valeur objet / poids dimension critique\n"
        "5] Tri croissant\n"
        "6] Tri dynamique\n"
        "7] Quitter\n\n\n\n"
    );
}

unsigned int launchLocalResearch(Parser *parser) {
    unsigned int returnVal = 0;
    unsigned int quit = 0;
    int userChoice = 0;
    int nbInstance = parser->nbInstance;
    int nbItems = Instance_getNbItems(parser->instanceArray[0]);
    Item** lclResearch = NULL;

    while (quit !=1) {
        printf("Utiliser la recherche locale ? \n1] Oui\n2] Non \n: ");
        scanf("%d", &userChoice);
        printf("user choice : %d\n", userChoice);
        if (userChoice == 1) {
            for (int idInstance = 0 ; idInstance < nbInstance ; idInstance++) {
                printf("La valeur de l'instance %d avant est : %d  \n",idInstance, objectiveFunction(parser->instanceArray[idInstance]->itemList, nbItems));
                lclResearch = localResearch(parser, idInstance);
                printf("La valeur de l'instance %d après est : %d  \n",idInstance, objectiveFunction(lclResearch , nbItems));
                Instance_ItemList_Delete(lclResearch, nbItems);    
            }
            returnVal = 1;
            quit = 1;
        } else if (userChoice == 2) {
            quit = 1;
        }
    }
    return returnVal;
}

void interface(Parser *parser) {
    unsigned int quitSoft = 0;
    unsigned int nbItems = 0;
    int userChoice = 0;
   
    while(quitSoft != 1){
        printChoiceOfSorting();
        printf("Choix : \n");
        scanf("%d", &userChoice);
      
        if (userChoice == 1) {
            for (int i = 0; i < parser->nbInstance; i++) {
                nbItems = Instance_getNbItems(parser->instanceArray[i]);
                fillBag(parser, randomSort(parser->instanceArray[i]), 0, i);
                printf("La valeur de l'instance %d : %d  \n",i,objectiveFunction(parser->instanceArray[i]->itemList, nbItems));
            } 
            if (launchLocalResearch(parser));
            for (int i = 0; i < parser->nbInstance; i++) {
                emptyBag(parser->instanceArray[i]);
            }
        } else if (userChoice == 2) {
            for (int i = 0; i < parser->nbInstance; i++) {
                nbItems = Instance_getNbItems(parser->instanceArray[i]);
                fillBag(parser, decreasingOrder(parser->instanceArray[i]), 0, i);
                printf("La valeur de l'instance %d : %d  \n",i,objectiveFunction(parser->instanceArray[i]->itemList, nbItems));
            } 
            if (launchLocalResearch(parser));
            for (int i = 0; i < parser->nbInstance; i++) {
                emptyBag(parser->instanceArray[i]);
            }
        } else if (userChoice == 3) {
            for (int i = 0; i < parser->nbInstance; i++) {
                nbItems = Instance_getNbItems(parser->instanceArray[i]);
                fillBag(parser, ratioOrderSum(parser->instanceArray[i]), 0, i);
                printf("La valeur de l'instance %d : %d  \n",i,objectiveFunction(parser->instanceArray[i]->itemList, nbItems));
            }
            if (launchLocalResearch(parser));
            for (int i = 0; i < parser->nbInstance; i++) {
                emptyBag(parser->instanceArray[i]);
            }
        } else if (userChoice == 4) {
            for (int i = 0; i < parser->nbInstance; i++) {
                nbItems = Instance_getNbItems(parser->instanceArray[i]);
                fillBag(parser, ratioOrderCritical(parser->instanceArray[i]), 0, i);
                printf("La valeur de l'instance %d : %d  \n",i,objectiveFunction(parser->instanceArray[i]->itemList, nbItems));
            }
            if (launchLocalResearch(parser));
            for (int i = 0; i < parser->nbInstance; i++) {
                emptyBag(parser->instanceArray[i]);
            }
        } else if (userChoice == 5) {
            for (int i = 0; i < parser->nbInstance; i++) {
                nbItems = Instance_getNbItems(parser->instanceArray[i]);
                fillBag(parser, increasingOrderItem(parser->instanceArray[i]), 0, i);
                printf("La valeur de l'instance %d : %d  \n",i,objectiveFunction(parser->instanceArray[i]->itemList, nbItems));
            }
            if (launchLocalResearch(parser));
            for (int i = 0; i < parser->nbInstance; i++) {
                emptyBag(parser->instanceArray[i]);
            }
        } else if (userChoice == 6) {
            for (int i = 0; i < parser->nbInstance; i++) {
                nbItems = Instance_getNbItems(parser->instanceArray[i]);
                fillBag(parser, ratioOrderCritical(parser->instanceArray[i]), 1, i);
                printf("La valeur de l'instance %d : %d  \n",i, objectiveFunction(parser->instanceArray[i]->itemList, nbItems));
            }
            if (launchLocalResearch(parser));
            for (int i = 0; i < parser->nbInstance; i++){
                emptyBag(parser->instanceArray[i]);
            }
        } else if (userChoice == 7) {
            quitSoft = 1;
        } else {
           printf("Entrée invalide !!!!!\n");
           quitSoft = 1;
        }
    }
}
