#include "./../include/parser.h"

Parser* Parser_new(FILE *file){
    Parser *newParser = (Parser*) malloc(sizeof(Parser));
    if (newParser) {
        if (Parser_init(newParser,file)) {
            free(newParser);
            newParser = NULL;
        } 
    }
    return newParser;
}

int Parser_init(Parser *parser, FILE *file) {
    if (parser) {
		parser->fIterator = FileIterator_new(file);
        parser->nbInstance = FileIterator_getNbInstance(parser->fIterator);
        parser->instanceArray = (Instance**) malloc(sizeof(Instance*) * parser->nbInstance);

		int nbItems = FileIterator_getNbItems(parser->fIterator);
		int nbDimensions = FileIterator_getNbDimensions(parser->fIterator);
        
		for (int i = 0; i < parser->nbInstance; i++) {
			parser->instanceArray[i] = Instance_new(nbItems, nbDimensions, parser->fIterator);
		}
       
        return 0;
    }
    return 1;
}

void Parser_finalize(Parser *parser){
    FileIterator_delete(parser->fIterator);
    for (int i = 0; i < parser->nbInstance; i++) {
        Instance_delete(parser->instanceArray[i]);
    }
    free(parser->instanceArray);
}


void Parser_delete(Parser *parser){
    if (parser) {
        Parser_finalize(parser);
        free(parser);
    }
}


